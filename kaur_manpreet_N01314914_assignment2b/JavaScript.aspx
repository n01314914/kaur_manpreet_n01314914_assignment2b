﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JavaScript.aspx.cs" Inherits="kaur_manpreet_N01314914_assignment2b.JavaScript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h3>Tricky concept in Java Script Is Objects</h3>
                        <p>
                            Object: In Programming we use objects as metaphors that keep related information together.<br />
                            Example of object In class:<br />
		                           <%-- var myCat={<br />
			                            name:"catty",<br />
			                            color:"grey",<br />
			                            age:5,<br />
			                            purr:function(){<br />
				                            alert("purrrrrrrrr");<br />
				                            }<br />
			                            }<br />--%>
                            
                            <usercontrol:codeBox ID="javascrpitcode" runat="server" SkinId="GridSkin" code="JavaScript" owner="example"></usercontrol:codeBox>

                        </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3>code written by me:</h3>
                        <p>
                            <%--var mycar=  {<br />
			                            name:"ford",<br />
			                            color:"silver",<br />
			                            model:2016,<br />
			                            drive:function(){<br />
				                            alert("i m driving");<br />
				                            }<br />
                                        }<br />--%>                            
                            
                            <usercontrol:codeBox ID="teacherjavascriptcode" runat="server" SkinId="GridSkin" code="JavaScript" owner="teacher"></usercontrol:codeBox>

                        </p>
                </div>
            </div>
    <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h3>Creating a JavaScript Object.</h3>
                        <p>
                           <%-- var person = {<br />
                                            firstName : "John",<br />
                                            lastName  : "Doe",<br />
                                            age       : 50,<br />
                                            eyeColor  : "blue"<br />
                                        }<br />--%>                    
                            
                            <usercontrol:codeBox ID="myjavascriptcode" runat="server" SkinId="GridSkin" code="JavaScript" owner="me"></usercontrol:codeBox>

                        </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3>Useful Resource links</h3>
                    <a href="https://www.w3schools.com/js/js_objects.asp">W3 Schools</a><br />
                    <a href="http://www.answers.com/Q/What_is_a_JavaScript_object">Answer.Com</a><br />
                </div>
    </div>
</asp:Content>

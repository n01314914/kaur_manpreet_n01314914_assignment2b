﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" Inherits="kaur_manpreet_N01314914_assignment2b.DigitalDesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-6 col-xs-12">
                        <h3>Tricky concept in HTML and CSS Is:  List</h3>
                            <p>
                                Defination of LIST:<br />
					            IN  html there are two main types of lists:<br /><br />
					
					            unordered lists (&lt;ul&gt;)  - the list items are marked with bullets<br />
					            ordered lists (&lt;ol&gt;) - the list items are marked with numbers or letters<br /><br />
					
					            The CSS list properties allow you to:<br />
					
					            Set different list item markers for ordered lists<br />
					            Set different list item markers for unordered lists<br />
					            Set an image as the list item marker<br />
					            Add background colors to lists and list items<br />
                            </p>
                    </div>
        <div class="col-md-6 col-xs-12">
                        <div>
                            <%--<h3>Code of List (in HTML) written by me:</h3>
                                used https://www.freeformatter.com to create html code for diplay purpose
                                    &lt;!DOCTYPE html&gt;<br />
					                &lt;html&gt;<br />
                                        &lt;head&gt;<br />
                                            &lt;title&gt;Demo list&lt;/title&gt;<br />
                                        &lt;/head&gt;<br />
					                    &lt;body&gt;<br />
					                    &lt;h2&gt;Ordered HTML list&lt;/h2&gt;<br />
                                        &lt;ol&gt;<br />
                                        &lt;li&gt;laptop&lt;/li&gt;<br />
                                        &lt;li&gt;moblie&lt;/li&gt;<br />
                                        &lt;li&gt;mac book&lt;/li&gt;<br />
                                        &lt;li&gt;PC&lt;/li&gt;<br />
                                        &lt;/ol&gt;<br />
					                    &lt;/body&gt;<br />
					                &lt;/html&gt;<br />

                            </div>
                        <div>
                            <h3>Code of List (in CSS) written by me:</h3>
                                <p>
                                    ol {<br />
					                    list-style-type:A;<br />
					                    background: #ff9999;<br />
					                    padding: 10px;<br />
				                    }<br />
                                </p>
                        </div>--%>               
                            
                            <usercontrol:codeBox ID="teacherdigitaldesigncode" runat="server" SkinId="dataGridSkin" code="DigitalDesign" owner="teacher"></usercontrol:codeBox>

                    </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-12">
                        <div>
                            <h3>Code of List (in HTML) Learn in class:</h3>
                                <%--used https://www.freeformatter.com to create html code for diplay purpose
                                    &lt;!DOCTYPE html&gt;<br />
					                &lt;html&gt;<br />
                                        &lt;head&gt;<br />
                                            &lt;title&gt;Demo list&lt;/title&gt;<br />
                                        &lt;/head&gt;<br />
					                    &lt;body&gt;<br />
					                    &lt;h2&gt;An unordered HTML list&lt;/h2&gt;<br />
					                    &lt;ul&gt;<br />
					                      &lt;li&gt;Coffee&lt;/li&gt;<br />
					                      &lt;li&gt;Tea&lt;/li&gt;<br />
					                      &lt;li&gt;Milk&lt;/li&gt;<br />
					                    &lt;/ul&gt;  <br />
					                    &lt;/body&gt;<br />
					                &lt;/html&gt;<br />
                            </div>
                        <div>
                            <h3>Code of List (in CSS) Learn in class:</h3>
                                <p>
                                    ul.demo {<br />
                                                list-style-type: circle;<br />
                                                margin: 0;<br />
                                                padding: 0;<br />
                                            }<br />
                                </p>
                        </div>--%>                
                            
                            <usercontrol:codeBox ID="mydigitaldesigncode" runat="server" SkinId="dataGridSkin" code="DigitalDesign" owner="me"></usercontrol:codeBox>

                    </div>

        <div class="col-md-6 col-xs-12">
                        <h3>Useful Resource links</h3>
                        <a href="https://www.w3schools.com/css/css_list.asp">W3 Schools</a><br />
                        <a href="https://www.smashingmagazine.com/2009/12/styling-html-lists-with-css-techniques-and-resources/">Smashing Magazine</a><br />
                        <a href="https://learn.shayhowe.com/html-css/creating-lists/">learn.shayhowe.com</a><br />
                    </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace kaur_manpreet_N01314914_assignment2b.UserControl
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }
        }
        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }
        }

        DataView CreateCode(List<string> codeList)
        {
            DataTable gridCode = new DataTable();
            DataColumn line = new DataColumn();
            DataColumn code = new DataColumn();

            DataRow coderow;

            line.ColumnName = "Line";
            line.DataType = System.Type.GetType("System.Int32");
            gridCode.Columns.Add(line);

            code.ColumnName = "Code";
            code.DataType = System.Type.GetType("System.String");
            gridCode.Columns.Add(code);


            int i = 0;
            foreach (string codeline in codeList)
            {
                coderow = gridCode.NewRow();
                coderow[line.ColumnName] = i;
                string final_code = System.Net.WebUtility.HtmlEncode(codeline);
                final_code = final_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code.ColumnName] = final_code;

                i++;
                gridCode.Rows.Add(coderow);
            }

            DataView codeview = new DataView(gridCode);
            return codeview;

        }

        protected void Page_Load(object sender, EventArgs e)
        {


            List<string> databaseteachercode = new List<string>(new string[]{
                "Select Orders.OrderID, Customers.CustomerName",
                "From Orders",
                "Innner Join Customers on Orders.CustomerID = Customers.CustomerID;"
            });

            List<string> databasemycode = new List<string>(new string[]{
                "select clients.clientfname,clients.clientlname, count(cars.carid)",
                "FROM carsxclients",
                "INNER JOIN cars ON cars.carid = carsxclients.carid",
                "inner join  clients ON carsxclients.clientid=clients.clientid",
                "group by clients.clientfname,clients.clientlname;"
            });

            List<string> javascriptcode = new List<string>(new string[]{
                "var myCat={",
                "~name:\"catty\",",
                "~color:\"grey\",",
                "~age:5,",
                "~purr:function(){",
                "~~alert(\"purrrrrrrrr\");",
                "~~}",
                "}"
            });

            List<string> javascriptteachercode = new List<string>(new string[]{
                "var myCat={",
                "~name:\"catty\",",
                "~color:\"grey\",",
                "~age:5,",
                "~purr:function(){",
                "~~alert(\"purrrrrrrrr\");",
                "~~}",
                "}"
            });

            List<string> javascript_my_code = new List<string>(new string[]{
                 "var person={",
                "~firstName:\"John\",",
                "~lastName:\"Doe\",",
                "~age:50,",
                "~eyeColor:\"blue\"",
                "}"
            });

            
            List<string> digitaldesignmycode = new List<string>(new string[]{
             "<!DOCTYPE html>",
             "<html>",
             "<body>",
             "~<h2>An unordered HTML list</h2>",
             "~<ol>",
             "~~<li>laptop</li>",
             "~~<li>moblie</li>",
             "~~<li>PC</li>",
             "~~<li>mac book</li>",
             "~</ol>",
             "</ body >",
             "</ html >",
             "",
             "<h3>Code of List (in CSS) Learn in class:</h3>",
             "~ul.demo",
             "~{",
             "~~list-style-type:A;",
             "~~background: #ff9999;",
             "~~padding: 10px;",
             "~}"
            });

            List<string> digitaldesignteachercode = new List<string>(new string[]{
             "<!DOCTYPE html>",
             "<html>",
             "<body>",
             "~<h2>An unordered HTML list</h2>",
             "~<ul>",
             "~~<li>Coffee</li>",
             "~~<li>Tea</li>",
             "~~<li>Milk</li>",
             "~</ul>",
             "</ body >",
             "</ html >",
             "",
             "<h3>Code of List (in CSS) Learn in class:</h3>",
             "~ul.demo",
             "~{",
             "~~list -style-type: circle;",
             "~~margin: 0;",
             "~~padding: 0;",
             "~}"
            });

            List<string> codetodisplay = new List<string>(new String[] { });

            if (code == "JavaScript" && owner == "example")
            {
                codetodisplay = javascriptcode;
            }
            else if (code == "JavaScript" && owner == "teacher")
            {
                codetodisplay = javascriptteachercode;
            }
            else if (code == "JavaScript" && owner == "me")
            {
                codetodisplay = javascript_my_code;
            }
            else if (code == "Database" && owner == "teacher")
            {
                codetodisplay = databaseteachercode;
            }
            else if (code == "Database" && owner == "me")
            {
                codetodisplay = databasemycode;
            }
            else if (code == "DigitalDesign" && owner == "teacher")
            {
                codetodisplay = digitaldesignteachercode;
            }
            else if (code == "DigitalDesign" && owner == "me")
            {
                codetodisplay = digitaldesignmycode;
            }

            DataView dv = CreateCode(codetodisplay);
            DataGridcontainer.DataSource = dv;
            DataGridcontainer.DataBind();

        }
    }
}
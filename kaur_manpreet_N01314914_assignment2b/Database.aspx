﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="kaur_manpreet_N01314914_assignment2b.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h3>Tricky concept is Advance joins in mySql</h3>
                        <p>
                            Defination:	<br />
					        A JOIN clause is used to combine rows from two or more tables, based on a related column between them.
                        </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3>Written by me:</h3>
                        <p>
                            <%--Select Orders.OrderID, Customers.CustomerName<br />
				            From Orders<br />
				            Innner Join Customers on Orders.CustomerID = Customers.CustomerID;<br />--%>          
                            
                            <usercontrol:codeBox ID="teacherdatabasecode" runat="server" SkinId="GridSkin" code="Database" owner="teacher"></usercontrol:codeBox>

                        </p>
                </div>
            </div>
    <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h3>Learn in the class:</h3>
                        <p>   
				            <%--select clients.clientfname,clients.clientlname, count(cars.carid)<br />
				            FROM carsxclients <br />
				            INNER JOIN cars ON cars.carid = carsxclients.carid <br />
				            inner join  clients ON carsxclients.clientid=clients.clientid <br />
				            group by clients.clientfname,clients.clientlname;<br />--%>
                            
                            <usercontrol:codeBox ID="my_database_code" runat="server" SkinId="GridSkin" code="Database" owner="me"></usercontrol:codeBox>

                        </p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3>Useful Resource links</h3>
                    <a href="https://www.w3schools.com/sql/sql_join_inner.asp">W3 Schools</a><br />
                    <a href="https://www.tutorialspoint.com/sql/sql-inner-joins.htm">Tutorials Point</a><br />
                </div>
    </div>
</asp:Content>